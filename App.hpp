#ifndef _APP_HPP_
#define _APP_HPP_

#include <string>
#include <fstream>
#include <iostream>
#include <ctime>
#include <unistd.h>

#include <nlohmann/json.hpp>

using json = nlohmann::json;

enum message_type {
    SUCCESS,
    ERROR,
    FATAL,
    WARNING,
    NOTIFY,
    MESSAGE
};

void LogMessage(message_type type , const char *message);
std::string ReadFileToString(const char *path);
std::string CurrentDateTime();
json ReadJSON(const char *path);
json CheckConfig(json config_data);

#endif /* _APP_HPP_ */