#include "App.hpp"

// Публикация сообщения в потоке, определяемом типом сообщения
void LogMessage(message_type type, const char *message){
    std::string _now = CurrentDateTime();

    switch (type){
        case SUCCESS:
            std::cout << "[SUCCESS][" << _now << "] " << message << "\n";
            break;
        case ERROR:
            std::cerr << "[ERROR][" << _now << "] " << message << "\n";
            break;
        case FATAL:
            std::cout << "[FATAL][" << _now << "] " << message << "\n";
            std::cerr << "[FATAL][" << _now << "] " << message << "\n";
            std::exit(1);
            break;
        case WARNING:
            std::cout << "[WARNING][" << _now << "] " << message << "\n";
            break;
        case NOTIFY:
            std::cout << "[NOTIFY][" << _now << "] " << message << "\n";
            break;
        default:
            std::cout << "[MESSAGE][" << _now << "] " << message << "\n";
    }
}

// Чтение файла в std::string
std::string ReadFileToString(const char *path){
    std::ifstream _file(path);

    if (!_file){
        std::string message("Error opening file: ");
        message += path;
        LogMessage(FATAL, message.c_str());
    }
    
    std::string _file_content(
        (std::istreambuf_iterator<char>(_file)),
        (std::istreambuf_iterator<char>()) 
    );
    return _file_content;
}

// Текущие дата и время в std::string
std::string CurrentDateTime(){
    time_t _time;
    struct tm * _time_data;
    char buffer[80];
    time(&_time);
    _time_data = localtime(&_time);
    strftime(buffer, sizeof(buffer), "%Y-%m-%d %H:%M:%S", _time_data);
    std::string result(buffer);

    return result;
}

// Чтение json-файла
json ReadJSON(const char *path){
    auto _json_data = json::parse(
        ReadFileToString(path)
    );

    return _json_data;
}

// Проверка конфигурации
json CheckConfig(json config_data){
    auto _config_data = config_data;

    _config_data["error_log"] = (
        _config_data["error_log"].is_null() || !_config_data["error_log"].is_string() ? 
        "error.log" : 
        _config_data["error_log"]);

    _config_data["success_log"] = (
        _config_data["success_log"].is_null() || !_config_data["success_log"].is_string() ? 
        "success.log" : 
        _config_data["success_log"]);
    
    return _config_data;
}