#ifndef _DAEMON_HPP_
#define _DAEMON_HPP_

#include <stdio.h> 
#include <sys/types.h> 
#include <unistd.h>
#include <sys/stat.h>
#include <sys/signal.h>

void Daemonize(const char *dir, const char *pidfile);
int Do();

#endif /* _DAEMON_HPP_ */