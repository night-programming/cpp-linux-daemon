#include <iostream>

#include "App.hpp"
#include "Daemon.hpp"

int main(int argc, char** argv){
    
    const char *_config_path;
    std::ofstream _success_log, _error_log;
    std::streambuf *orig_cout, *orig_cerr;

    if (argc < 2) {
        _config_path = "config.json";
    }
    else {
        _config_path = argv[1];
    }

    auto _config = CheckConfig(
        ReadJSON(_config_path)
    );

    _success_log.open(_config["success_log"], std::ofstream::out | std::ofstream::app);
    _error_log.open(_config["error_log"], std::ofstream::out | std::ofstream::app);

    if (_success_log.is_open()){
        orig_cout = std::cout.rdbuf();
        std::cout.rdbuf(_success_log.rdbuf());
    }
    
    if (_error_log.is_open()){
        orig_cerr = std::cerr.rdbuf();
        std::cerr.rdbuf(_error_log.rdbuf());
    }
    
    Daemonize("/", _config["pid_file"].dump().c_str());

    if (_success_log.is_open()){
        std::cout.rdbuf(orig_cout);
        _success_log.close();
    }
    
    if (_error_log.is_open()){
        std::cerr.rdbuf(orig_cerr);
        _error_log.close();
    }

    return 0;
}