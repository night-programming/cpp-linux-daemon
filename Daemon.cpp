#include "App.hpp"
#include "Daemon.hpp"

int Do(){
    while (1)
    {
        LogMessage(MESSAGE, "This code is executed in the daemon!");
        sleep (20);
        break;
    }

    LogMessage(MESSAGE, "Daemon finished work");
    
    return 0;
}

void Daemonize(const char *dir, const char *pidfile){
    int _pid, _sid, _status;

    _pid = fork();

    if (_pid < 0){
        // Ошибка форка процесса
        LogMessage(FATAL, "Unable to demonize process");
    }
    else if (_pid == 0){
        // Дочерний процесс
        _sid = setsid();
        if (_sid < 0){
            LogMessage(FATAL, "Error creating session for process");
        }

        _pid = fork();
        if (_pid < 0){
            LogMessage(FATAL, "Unable to demonize process");
        }
        else if (_pid == 0){
            umask(0);
            chdir(dir);        
            
            _status = Do();
            
            std::exit(_status);
        }
        else{
            std::exit(0);
        }
    }
    else{
        // Процесс демонизирован. Родителя можно закрывать
        LogMessage(SUCCESS, "Process successfully demonized");
        std::exit(0);
    }
}